//app.js
const utils = require("pages/index/common.js");
App({
  onLaunch: function (options) {

    // 判断是否由分享进入小程序
    if (options.scene == 1007 || options.scene == 1008) {
      this.globalData.share = true
    } else {
      this.globalData.share = false
    };

    this.globalData.appid = wx.getAccountInfoSync().miniProgram.appId;
    
    wx.getSystemInfo({
      success: e => {
        // console.log(e)
        this.globalData.mobileModel = e.model;//获取机型
        this.globalData.height = e.statusBarHeight;

        this.globalData.screenHeight = e.screenHeight;
        this.globalData.windowWidth = e.windowWidth;
        this.globalData.windowHeight = e.windowHeight;
        //正式地址
         this.globalData.URL = 'https://xxxxxxxx:8080/ionia';
        //测试地址
        // this.globalData.URL = 'https://192.168.1.103:7789/ionia';

        // this.globalData.wsURL = 'ws://192.168.1.103/ionia/webSocket/';
      }
    })

    //检查用户登录态
    this.checkLoginSession();
    
    // wx.setTabBarBadge({
    //   index: 0,
    //   text: '99'
    // })

  },
   /**
     * 全局配置
     */
  globalData: {
    share: false,  // 分享默认为false
    height: 0,
    mobileModel:'',
    appid: '',
    // session_key: '',
    imgTypes: [],//图片分类，在首页js中进行初始化赋值
    authorization: {},
    miniName: '青衣图吧',
    appFlag: 'Nfzwyxy-20210420081820',

  },
  //检查登录态
  checkLoginSession: function() {
    var _this = this;
    wx.checkSession({
      success () {
        //session_key 未过期，并且在本生命周期一直有效,保存到全局
        wx.getStorage({
          key: 'session_key',
          success(res) {

            if(!res.data || res.data=="" || res.data==null) {
              _this.wxlogin_();
            }else{
              _this.globalData.session_key = res.data;
            }

            //是否触发index的回调函数
            if(_this.skeyCallback) {
              _this.skeyCallback(res.data);
            }
          },
          fail(e){
            _this.wxlogin_();
          }
        })
      },
      fail () {
        // session_key 已经失效，需要重新执行登录流程
        console.log('登录态已经失效')
        _this.wxlogin_();
      }
    })
  },

  wxlogin_ : function() {
    var _this = this;
    wx.login({
      success (res) {
        if (res.code) {
          //发起网络请求
          utils.post(_this.globalData.URL+'/mini/'+_this.globalData.appid+'/loginStatus?code='+res.code,
            {}).then(res1=>{
              if(res1.data.res==="ok") {
                
                wx.setStorage({
                  key:"session_key",
                  data:res1.data.skey
                })
                _this.globalData.session_key = res1.data.skey;
                //是否触发index的回调函数
                if(_this.skeyCallback) {
                  _this.skeyCallback(_this.globalData.session_key);
                }
              }else{
                console.log("登录失败！"+res1.data)
              }
            })
        } else {
          console.log('登录失败！' + res.errMsg)
        }
      }
    })
  }

})
