// pages/uploadImage/index.js
const app = getApp();
const util = require("../index/common.js")

Page({

  /**
   * 页面的初始数据
   */
  data: {
    //加载动画
    loadModal: false,

    // 组件所需的参数
    nvabarData: {
      showCapsule: 1, //是否显示左上角图标   1表示显示    0表示不显示
      title: '图片上传', //导航栏 中间的标题
      bgColors: '#6ABF43',
    },
    // 此页面 页面内容距最顶部的距离
    height: app.globalData.height * 2 + 20 ,

    maxCount: 16,//上传图片总数
    listData: [],//多图片
    size: 4,
    isIphoneX: false,

    imgList1: [],//预览图
    parentid: '',//预览图id

    isPreview: 0,//是否有预览权限 0有 1没有

    isPrivate: 0,//是否是私密图片 1是 0否
    isPwd:'',//私密图片密码
    
    modalName: null,
    textareaAValue: '',
    
    typePicker: [],
    typeIndex: 0,//分类下标
    typeno: 7,//选中的分类编号

  },
  //是否开启预览权限
  setPreview: function(e) {
    // console.log(e.detail.value)
    if(e.detail.value) {
      this.setData({
        isPreview: 0
      })
    }else{
      this.setData({
        isPreview: 1
      })
    }
  },
  //开启私密权限
  setPrivate: function(e) {
    // console.log(e.detail.value)
    if(e.detail.value) {
      this.setData({
        isPrivate: 1
      })
    }else{
      this.setData({
        isPrivate: 0
      })
    }
  },
  //保存输入密码
  pwdInput(e) {
    this.setData({
      isPwd: e.detail.value
    })
  },
  //选择分类
  PickerChange(e) {
    this.setData({
      typeIndex: e.detail.value,
      typeno: this.data.typePicker[e.detail.value].typeno,
    })
  },
  ChooseImage() {
    var that = this;
    wx.chooseImage({
      count: that.data.maxCount-that.data.listData.length, //默认9
      sizeType: ['original', 'compressed'], //可以指定是原图还是压缩图，默认二者都有
      // sizeType: ['compressed'], 
      sourceType: ['album','camera'], //从相册选择
      success: (res) => {
        if (that.data.listData.length != 0) {
          var arrs = that.toArray(res.tempFilePaths)
          that.setData({
            listData: that.data.listData.concat(arrs)
          })
          this.drag.dataChange();
        } else {
          var arrs = that.toArray(res.tempFilePaths)
          that.setData({
            listData: arrs
          })
          this.drag.dataChange();
        }
      }
    });
  },
  toArray(arr) {
    for(var i=0;i<arr.length;i++) {
      var obj = {
        title: "",
        description: "",
        images: arr[i],
        fixed: false
      }
      arr[i] = obj;
    }
    return arr
  },
  ViewImage(e) {
    wx.previewImage({
      urls: this.data.imgList,
      current: e.currentTarget.dataset.url
    });
  },
  DelImg(e) {
    wx.showModal({
      title: '温馨提示',
      content: '确定删除？',
      cancelText: '取消',
      confirmText: '删除',
      success: res => {
        if (res.confirm) {
          this.data.listData.splice(e.detail.newKey,1)
          this.setData({
            listData: this.data.listData
          });
          this.drag.dataChange();
        }
      }
    })
  },
  change(e) {
		this.setData({
			listData: e.detail.listData
		});
	},

	itemClick(e) {
    
  },
  //以下是预览图方法
  ChooseImage1() {
    var that = this;
    wx.chooseImage({
      count: 1, //默认9
      sizeType: ['original', 'compressed'], //可以指定是原图还是压缩图，默认二者都有
      // sizeType: ['compressed'], 
      sourceType: ['album','camera'], //从相册选择
      success: (res) => {
        if (res.tempFilePaths.length > 0) {
          that.setData({
            imgList1: that.data.imgList1.concat(res.tempFilePaths),
          })
          //上传预览图
          wx.uploadFile({
            url: app.globalData.URL+'/mini/imgOneupload',
            filePath: res.tempFilePaths[0],
            name: 'files',
            formData: {
              description: '',
              type: that.data.typeno,
              skey: app.globalData.session_key
            },
            success: function(res) {
              var data = JSON.parse(res.data);
              if(data.status==="noLogin") {
                wx.showToast({
                  title: "请先更新登录信息！",
                  duration: 1000,
                  icon: 'none',
                  mask: true
                })
                return;
              }
              that.setData({
                parentid: data.parentid,
              })
            },
            fail: function() {}
          })
        } else {
          that.setData({
            imgList1: res.tempFilePaths
          })
        }
      }
    });
  },
  ViewImage1(e) {
    wx.previewImage({
      urls: this.data.imgList1,
      current: e.currentTarget.dataset.url
    });
  },
  DelImg1(e) {
    wx.showModal({
      title: '温馨提示',
      content: '确定删除？',
      cancelText: '取消',
      confirmText: '确定',
      success: res => {
        if (res.confirm) {
          this.data.imgList1.splice(e.currentTarget.dataset.index, 1);
          this.setData({
            imgList1: this.data.imgList1,
            parentid: '',
          })
        }
      }
    })
  },
  textareaAInput(e) {
    this.setData({
      textareaAValue: e.detail.value
    })
  },
  //上传图片到服务器
  imgUpload: function(e) {
    //是否订阅模板消息,此方法需使用真机调试（在工具栏上面的真机调试）
    // wx.requestSubscribeMessage({
    //   tmplIds: ['xvxirwxjZexr6YGMIt8IYXRQ2xuzdr6khRmAV2bnadE','gbTPmWgpwb39x8b3QS4dol5wMmMEk5dN_fhWtfmgkfc'],
    //   success(res){
    //     console.log(res)
    //   },
    //   fail(error){
    //     console.log(error)
    //   },
    //   complete(){}
    // })
    var that = this;

    if(that.data.parentid === '') {
      wx.showToast({
        title: "预览图不能为空！",
        duration: 1000,
        icon: 'none',
        mask: true
      })
      return;
    }
    if(util.trim(that.data.textareaAValue) === '') {
      wx.showToast({
        title: "标题不能为空！",
        duration: 1000,
        icon: 'none',
        mask: true
      })
      return;
    }
    //密码校验
    if(that.data.isPrivate==1 && util.trim(that.data.isPwd)==='' ) {
      wx.showToast({
        title: "私密图片密码不能为空！",
        duration: 1000,
        icon: 'none',
        mask: true
      })
      return;
    }

    if(that.data.listData.length == 0) {
      wx.showToast({
        title: "请上传至少一张图片！",
        duration: 1000,
        icon: 'none',
        mask: true
      })
      return;
    }

    //显示加载动画
    that.setData({
      loadModal: true
    })

    var imgLists = that.data.listData;
    var completeCount = 0;
    //注意listData是对象数组： [{
    //     title: "标题",
    //     description: "描述",
    //     images: "图片地址",
    //     fixed: false
    //   }]
    for( var i=0;i<imgLists.length;i++) {
      var imgIndex = i+1;
      wx.uploadFile({
        url: app.globalData.URL+'/mini/imgupload',
        filePath: imgLists[i].images,
        name: 'files',
        formData: {
          parentid: that.data.parentid,
          type: that.data.typeno,
          desc: that.data.textareaAValue,
          isPreview: that.data.isPreview,
          uploadTime: imgIndex+'',
          skey: app.globalData.session_key,
          isPrivate:that.data.isPrivate,
          isPwd:that.data.isPwd
        },
        success(res1) {
          // console.log(res1)
          if(res1.status==="noLogin") {
            wx.showToast({
              title: "请先更新登录信息！",
              duration: 1000,
              icon: 'none',
              mask: true
            })
            return;
          }
          completeCount++
          //隐藏加载动画
          if(completeCount == imgLists.length) {
            that.setData({
              loadModal: false
            })
          }
          if(completeCount == imgLists.length) {
            wx.showModal({
              content: '图片上传完成，刷新查看！',
              showCancel: false,
              confirmText: '确定',
              success: res => {
                if (res.confirm) {
                  wx.switchTab({
                    url: '../index/index',
                  })
                }
              }
            })
          }
        },
        complete(e){
          
        },
        fail: function(error) {
          wx.showToast({
            title: "网络不好，请稍后再试！",
            duration: 1000,
            icon: 'none',
            mask: true
          })
        }
      })
    }
  },

  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options) {
    this.drag = this.selectComponent('#drag');
    
    //分类模块赋值
    this.setData({
      typePicker: app.globalData.imgTypes,
      typeno: app.globalData.imgTypes[0].typeno,
    })
  },

})