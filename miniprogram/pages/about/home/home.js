const app = getApp();
const common = require("../../index/common.js");
// const defaultAvatarUrl = 'https://mmbiz.qpic.cn/mmbiz/icTdbqWNOwNRna42FI242Lcia07jQodd2FJGIYQfG0LAJGFxM4FbnQP6yfMxBgJ0F3YRqJCJ1aPAK2dQagdusBZg/0';
Component({
  options: {
    addGlobalClass: true,
  },
  data: {
    // avatarUrl: defaultAvatarUrl,
    avatarUrl: app.globalData.authorization.avatarurl,
    nickName: app.globalData.authorization.nickName,
    iconList: [{
      icon: 'notice',
      color: 'olive',
      badge: 0,
      name: '违规评论'
    }, {
      icon: 'pic',
      color: 'yellow',
      badge: 0,
      name: '图像'
    }, {
      icon: 'attention',
      color: 'cyan',
      badge: 0,
      name: '今日访问'
    }, {
      icon: 'attention',
      color: 'blue',
      badge: 0,
      name: '历史访问'
    }, {
      icon: 'people',
      color: 'red',
      badge: 0,
      name: '用户数量'
    }, {
      icon: 'favor',
      color: 'yellow',
      badge: 0,
      name: '已收藏'
    }],
    gridCol:3,
    gridBorder: true,

    showModal: false,//模态框
    managerPassword: 'huang',
    inputValue: '',//输入的值

    starCount: 0,
    shareCount: 0,
    visitTotal: 0,
    UCenterBackgroundImg: '',//背景图
    zanshangCode: '',//赞赏码

    textareaAValue: '',//测试
  },
  
  attached() {
    let that = this;
    wx.showLoading({
      title: '数据加载中',
      mask: true,
    })
    let i = 0;
    numDH();
    function numDH() {
      if (i < 20) {
        setTimeout(function () {
          that.setData({
            starCount: i,
            shareCount: i,
            visitTotal: i
          })
          i++
          numDH();
        }, 20)
      } else {
        that.setData({
          starCount: that.coutNum(app.globalData.starCount),
          shareCount: that.coutNum(app.globalData.shareCount),
          visitTotal: that.coutNum(app.globalData.visitTotal),
        })
      }
    }
    wx.hideLoading()
  },
  methods: {
    coutNum(e) {
      if (e > 1000 && e < 10000) {
        e = (e / 1000).toFixed(1) + 'k'
      }
      if (e > 10000) {
        e = (e / 10000).toFixed(1) + 'W'
      }
      return e
    },
    CopyLink(e) {
      wx.setClipboardData({
        data: e.currentTarget.dataset.link,
        success: res => {
          wx.showToast({
            title: '已复制',
            duration: 1000,
          })
        }
      })
    },
    onChooseAvatar(e) {
      console.log(e)
      const { avatarUrl } = e.detail 
      this.setData({
        avatarUrl,
      })
    
    },
    showModal(e) {
      this.setData({
        modalName: e.currentTarget.dataset.target
      })
    },
    hideModal(e) {
      this.setData({
        modalName: null
      })
    },
    showQrcode() {
      var that = this;

      wx.previewImage({
        urls: [that.data.zanshangCode],
        current: that.data.zanshangCode, // 当前显示图片的http链接      
      })
    },
    onLoad: function() {
      
      this.setData({
        authLevel: app.globalData.authorization.authLevel,
        UCenterBackgroundImg: app.globalData.centerImg,
        zanshangCode: app.globalData.zanshangCode,
        managerPassword: app.globalData.managerPwd,
        showUploadBtn: app.globalData.showUploadBtn,
      })
      this.reloadIt();
      // this.startConnect();
    },
    onShow() {
      this.reloadIt();
    },

    //加载实时数据
    reloadIt: function() {
      let that = this;
      
      common.post(app.globalData.URL+'/mini/intime/'+app.globalData.session_key,
      {}).then(res=>{
        if(res) {
          var data = res.data;
          // console.log(data)
          that.data.iconList[0].badge=data.comCount;
          that.data.iconList[1].badge=that.coutNum(data.picCount);
          that.data.iconList[2].badge=that.coutNum(data.todayCount);
          that.data.iconList[3].badge=that.coutNum(data.hisCount);
          that.data.iconList[4].badge=data.userCount;
          that.data.iconList[5].badge=that.coutNum(data.favorCount);

          that.setData({
            iconList: that.data.iconList
          })
        }
      })
    },

    //创建websocket连接
    // startConnect: function () {
    //   let that = this;
    //   //本地测试使用 ws协议 ,正式上线使用 wss 协议
    //   var wxst = wx.connectSocket({
    //     url: app.globalData.wsURL+app.globalData.session_key,
    //     header:{
    //       'content-type': 'application/json'
    //     },
    //   });
    //   //（真机无法触发！！！！？？？、）
    //   wxst.onOpen(res => {
    //     // console.info('连接打开成功');
    //     // wx.sendSocketMessage({
    //     //   data:'发送给服务器'
    //     // })
    //     // wxst.send({
    //     //   data:'发送给服务器',
    //     //   success:function() {
    //     //   },
    //     //   complete:function() {
    //     //   }
    //     // });
    //     this.reloadIt();
        
    //   });
      
    //   wxst.onError(res => {
    //     console.log(res);
    //   });

    //   //接收服务器消息（真机无法触发！！！！？？？、）
    //   wxst.onMessage(res => {

    //     var data = common.str2json(res.data);
    //     // console.log(data)
    //     that.data.iconList[0].badge=data.comCount;
    //     that.data.iconList[1].badge=data.picCount;
    //     that.data.iconList[2].badge=data.todayCount;
    //     that.data.iconList[3].badge=data.hisCount;
    //     that.data.iconList[4].badge=data.userCount;
    //     that.data.iconList[5].badge=data.favorCount;
    //     that.setData({
    //       iconList: that.data.iconList,
    //     })
        
    //   });

    // },

    SetWaterMark: function(e){
      app.globalData.authorization.waterMarkIs = !(e.detail.value)
    },
    SetPreview: function(e){
      app.globalData.authorization.previewImgIs = e.detail.value
    },

    //用户须知
    showNotice(e) {
      this.setData({
        modalName: e.currentTarget.dataset.target
      })
    },
    hideNotice(e) {
      this.setData({
        modalName: null
      })
    },

    //密码模态框
    inputChange: function(e) {
      this.setData({
        inputValue: e.detail.value
      })
    },
    showDialogBtn: function() {
      this.setData({
        showModal: true
      })
    },
    preventTouchMove: function () {
    },
    hideModal: function () {
      this.setData({
        showModal: false
      });
    },
    onCancel: function () {
      this.hideModal();
    },
    onConfirm: function () {
      var pwd = this.data.managerPassword;
      var val = this.data.inputValue;
      if(pwd===val) {
        this.setData({
          isManager: true,
        })
        this.hideModal();
      }else{
        wx.showToast({
          title: "密码错误",
          duration: 1000,
          icon: 'none',
          mask: true
        })
      }
    }

  }
})