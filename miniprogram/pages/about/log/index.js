const app = getApp();
Page({
  data: {

    // 组件所需的参数
    nvabarData: {
      showCapsule: 1, //是否显示左上角图标   1表示显示    0表示不显示
      title: '开发日志', //导航栏 中间的标题
      bgColors: '#6ABF43'

    },
    // 此页面 页面内容距最顶部的距离
    height: app.globalData.height * 2 + 20 ,

    StatusBar: app.globalData.StatusBar,
    CustomBar: app.globalData.CustomBar,
    ColorList: app.globalData.ColorList,    
  },
  onLoad: function () { },
  pageBack() {
    wx.navigateBack({
      delta: 1
    });
  }
});
