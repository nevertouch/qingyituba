// pages/authorize/login/index.js
const utilApi = require("../../index/common");
const app = getApp();
Page({

  /**
   * 页面的初始数据
   */
  data: {
    // canIUse: wx.canIUse('button.open-type.getUserInfo'),

    //顶部navbar
    nvabarData: {
      showCapsule: 1, //是否显示左上角图标   1表示显示    0表示不显示
      title: '授权登录', //导航栏 中间的标题
      bgColors: '-webkit-linear-gradient(top,rgba(0,0,0,.5),rgba(0,0,0,0))'
    },
    // 此页面 页面内容距最顶部的距离
    height: app.globalData.height * 2 + 20 ,  

    canIUseGetUserProfile: false,
  },

  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options) {
    if (wx.getUserProfile) {
      this.setData({
        canIUseGetUserProfile: true
      })
    }
  },

  getUserProfile: function (res1) {
    var pages = getCurrentPages();
    var beforePage = pages[pages.length - 2]; // 前一个页面
    

    wx.getUserProfile({
      desc: '用于完善基础信息', // 声明获取用户个人信息后的用途，后续会展示在弹窗中，请谨慎填写
      success: (res) => {
        //用户按了允许授权按钮
        // console.log(res)
        if (res.userInfo) {
          // 获取用户信息
          var data = res.userInfo;
          //保存到数据库
          wx.login({
            success (res2) {
              if (res2.code) {
                data.code = res2.code;
                //因为微信小程序改版之后授权信息没有了头像和昵称，需要手动获取并上传到服务器保存
                //data.avatarUrl="";
                //data.nickName="";
                //发起网络请求
                utilApi.post(app.globalData.URL+'/mini/'+app.globalData.appid+'/savewxuser',
                data,{ 'content-type': 'application/json'}).then(res1=>{
                    // console.log('授权成功回调结果：');
                    // console.log(res);
                    if(res1.data.res==="ok") {
                        wx.setStorage({
                          key:"session_key",
                          data:res1.data.skey
                        })
                        //保存到全局
                        app.globalData.session_key = res1.data.skey;
                        app.globalData.authorization.authLevel = res1.data.level;//用户权限等级
                        // beforePage.onLoad();
                        beforePage.setData({
                          authLevel: res1.data.level
                        })

                        wx.showToast({
                          title: "登录成功，右上角刷新",
                          duration: 2000,
                          icon: 'none',
                          mask: true
                        })
                    }else{
                      console.log("登录失败！"+res1.data)
                    }
                  })
              } else {
                console.log('登录失败！' + res2.errMsg)
              }
            }
          })
          //返回上一页
          // wx.navigateBack();
          //跳转到首页
          wx.switchTab({
            url: '../../index/index'
          })
        }
      },
      fail: function(result) {
        //用户按了拒绝按钮
        wx.showModal({
          title: '温馨提示',
          content: '您拒绝授权，将无法使用小程序功能，请授权之后再进入!',
          showCancel: false,
          confirmText: '返回授权',
          success: function (res) {
            // 用户没有授权成功，不需要改变 isHide 的值
            if (res.confirm) {
              // console.log('用户点击了“返回授权”');
            }
          }
        });
      }
      
    })
  }

})