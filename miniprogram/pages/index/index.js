//index.js
var utils = require('common.js')
var app = getApp()

Page({

  // tab切换 
  swichNav: function (e) {
    var that = this;
    if (that.data.currentTab == e.target.dataset.current) {
      return false;
    } else {
      that.setData({
        currentTab: e.target.dataset.current,
        selectType: e.currentTarget.dataset.no,
      })
    }
  },
  //swiper滑动事件
  swiperChange: function (e) {
    var type = this.data.barList[e.detail.current].typeno;//分类id
    var str = utils.trim(this.data.inputVal);//搜索框
    var selectItem = this.data.allImgList[e.detail.current];//分类项数据
    
    //搜索框为空且该分类数据已加载过，则不再查询
    if(str==='' && selectItem && (selectItem.leftList.length > 0 || selectItem.rightList.length > 0)) {
      this.setData({
        currentTab: e.detail.current, //分类的下标
        scrollLeft: (e.detail.current-1)*60, //左边距
        selectType: type, //选中的分类
      })
      return;
    }
   
    var fData = {};//请求参数
    if(str==='') {
      fData = {pageIndex: 1,pageSize: 10,type:type}
    }else{
      fData = {pageIndex: 1,pageSize: 10,s: str,type:type}
      //如果搜索框不为空则重新加载数据
      this.data.allImgList[e.detail.current].leftList=[];
      this.data.allImgList[e.detail.current].rightList=[];
      this.data.allImgList[e.detail.current].pageNo=1;
      this.data.allImgList[e.detail.current].itemCount=0;
      this.setData({
        allImgList: this.data.allImgList,
      })
    }

    //更新左右两栏的数据以及累计高度
    this.setData({
      currentTab: e.detail.current, //分类的下标
      scrollLeft: (e.detail.current-1)*60, //左边距
      selectType: type, //选中的分类
    })
    this.queryData(fData,e.detail.current);

  },

  //页面的初始数据
  data: {
    // 此页面 页面内容距最顶部的距离
    height: app.globalData.height * 2 + 15 ,
    // clientHeight_: app.globalData.windowHeight,

    showModal: false,//模态框
    inputValue: '',//输入的值

    //回到顶部
    showTop: true,

    currentTab: 0,//被选中导航栏下标

    scrollLeft:0,//导航栏距离左边距离
    inputShowed: false,//搜索框
    inputVal: "",

    topBarBackground:'#f7f7f7',//导航栏背景色#1f1f1f
    topBarFontColor: '#666',//导航栏字体颜色
    descColor:'#404d5b',//图片描述-文字颜色
    descFontSize:'26rpx',//图片描述-字体大小
    descBgColor: '#fff',//图片描述-背景颜色
    picShaDow: 'none',//图片项-阴影
    pageBackgroundimg:'',
    itemBgColor: "#fff",//图片项-背景颜色
    
    pageSize:10,//条数

    noPicColor: '#000',

    barList: [],

    // allImgList: [],//分类数组
  },

    //这里先加载初始化数据，加载初始化样式（后台配置）
  onLoad: function(options) {
    var that = this;

    var skey = app.globalData.session_key;
    if(skey && skey!='') {
      that.initData(skey);
    }else{
      app.skeyCallback = skey =>{
        if(skey!='') {
          that.initData(skey);
        }
      }
    }

  },
  initData: function(skey) {
    var that = this;
    wx.request({
      url: app.globalData.URL+'/mini/pageStyle?skey='+skey,
      method: 'POST', // OPTIONS, GET, HEAD, POST, PUT, DELETE, TRACE, CONNECT 
      data: {},
      success: function(res) {
        if(res.data) {
          var obj = res.data;
          var allImgList_ = new Array(obj.typelist.length);//初始化数组长度
          var allHeight_ = new Array(obj.typelist.length);//初始化数组长度（高度）
          for (var k = 0; k < obj.typelist.length; k++) {
            allImgList_[k]={
              'itemCount': 0,'leftList':[],'rightList':[],'pageNo': 1
            }
          }

          that.setData({
            topBarBackground: obj.styletopbarbgcolor?obj.styletopbarbgcolor:that.data.topBarBackground,//导航栏背景色#f7f7f7
            topBarFontColor: obj.topBarFontColor?obj.topBarFontColor:that.data.topBarFontColor,//导航栏分类字体颜色
            descColor: obj.styledescColor?obj.styledescColor:that.data.descColor,//图片描述-文字颜色
            descFontSize: obj.styledescFontSize?obj.styledescFontSize:that.data.descFontSize,//图片描述-字体大小
            pageBackgroundimg: obj.stylebackgroundImg,//页面背景图
            noPicColor: obj.stylebackgroundImg?'#fff':'#000',
            picShaDow: obj.picShadow?obj.picShadow:'none',//阴影
            descBgColor: obj.descBgColor?obj.descBgColor:'#fff',
            itemBgColor: obj.itemBgColor?obj.itemBgColor:'#fff',
            
            barList: obj.typelist,//分类
            selectType: obj.typelist[0].typeno, //选中第一个分类
            allImgList: allImgList_ ,
            allHeight: allHeight_,
          })
          /**
           * 初始化到全局变量
           */
          app.globalData.imgTypes = obj.typelist;//分类数据
          app.globalData.authorization.authLevel = obj.authLevel;//用户权限等级
          app.globalData.authorization.avatarurl = obj.avatarurl;//用户头像
          app.globalData.authorization.nickName = obj.nickName;//用户昵称
          
          //赞赏码和个人中心背景图,管理员密码
          app.globalData.zanshangCode = obj.zanshangCode;
          app.globalData.centerImg = obj.centerImg;
          app.globalData.managerPwd = obj.managerPwd;
          //浏览次数，收藏数量，分享次数
          app.globalData.starCount = obj.starCount;
          app.globalData.shareCount = obj.shareCount;
          app.globalData.visitTotal = obj.visitTotal;
          //违规评论
          app.globalData.violateComment = obj.violateComment;
          //是否显示图片上传按钮
          app.globalData.showUploadBtn = obj.openUploadImg;
          //设置tabbar样式
          wx.setTabBarStyle({
            color: obj.tabbarColor?obj.tabbarColor : '#AAAAAA',
            selectedColor: obj.tabbarSelColor?obj.tabbarSelColor : '#5ABC46',
            backgroundColor: obj.tabbarBgColor?obj.tabbarBgColor : '#fff',
            borderStyle:obj.tabbaBorderColor?obj.tabbaBorderColor : 'white',
          })

          setTimeout(() => {
            //请求初始化数据
            that.queryData({
              pageIndex: 1,
              pageSize: that.data.pageSize
            },0);
          }, 100);
        }
      },
      fail: function() {
        wx.showToast({
          title: "初始化请求失败！",
          duration: 1000,
          icon: 'none',
          mask: true
        })
      }
    })
    
  },

  //远程加载图片
  queryData: function (formData,typeIndex,flag) {
    var that = this;
    wx.request({
      url: app.globalData.URL+'/mini/thumbList?app='+app.globalData.appFlag,
      method: 'POST', // OPTIONS, GET, HEAD, POST, PUT, DELETE, TRACE, CONNECT 
      header: {
        'Content-Type': "application/x-www-form-urlencoded",
        'Cookie': 'SESSION=' + wx.getStorageSync("sessionId")
      }, // 设置请求的 header
      data: formData,
      success: function(res) {
        if(res.data) {
          var obj = res.data;
          that.waterFullFlow(obj.piclist,typeIndex,obj.totalPage,flag);

        }else{
          wx.showToast({
            title: "没有更多数据了",
            duration: 1000,
            icon: 'none',
            mask: true
          })
        }
      },
      fail: function() {
        wx.showToast({
          title: "请求失败！",
          duration: 1000,
          icon: 'none',
          mask: true
        })
      },
      complete: function() {
        // complete 
        wx.stopPullDownRefresh();//停止下拉刷新
      }
    })
  },
  /**
   * 瀑布流布局
   * 所有数据，分类下标，总页数
   */
  waterFullFlow: function (allData,typeIndex,totalPage,flag) {
    var that = this;

    var leftList = [];
    var rightList = [];
    var leftHeight = 0, rightHeight = 0;
    var allImgList = that.data.allImgList;//已经加载的数据

    for (let i = 0; i < allData.length; i++) {
      var coverHeight1 = parseInt(allData[i].coverHeight);
      var coverWidth1 = parseInt(allData[i].coverWidth);
      //css中固定图片宽325,计算等比的高度
      var currentItemHeight = parseInt(Math.round(coverHeight1 * 325 / coverWidth1));
      allData[i].coverHeight = currentItemHeight + "rpx";//因为xml文件中直接引用的该值作为高度，所以添加对应单位
      allData[i].halfHeight = currentItemHeight / 2 - 4;//图片一半高度（用于垂直居中文字）

      if (leftHeight == rightHeight || leftHeight < rightHeight) {//判断左右两侧当前的累计高度，来确定item应该放置在左边还是右边
        leftList.push(allData[i]);
        leftHeight += currentItemHeight;
      } else {
        rightList.push(allData[i]);
        rightHeight += currentItemHeight;
      }
    }
    //更新左右两栏的数据以及累计高度
    if(flag!='refresh') {
      //在原来数据的基础上拼接数据
      allImgList[typeIndex] = {
        'itemCount': totalPage,
        'leftList': allImgList[typeIndex].leftList.concat(leftList),
        'rightList': allImgList[typeIndex].rightList.concat(rightList),
        'pageNo': allImgList[typeIndex].pageNo,
      };
    }else{
      //如果是刷新则只加载第一页数据（取消搜索框的时候）
      allImgList[typeIndex] = {
        'itemCount': totalPage,
        'leftList': leftList,
        'rightList': rightList,
        'pageNo': allImgList[typeIndex].pageNo,
      };
    }
    
    that.setData({
      allImgList: allImgList,
    })

    //根据内容，动态更新swiper高度
    wx.createSelectorQuery().select("#imgContent"+that.data.currentTab).boundingClientRect().exec(function(res) {
      that.data.allHeight[typeIndex] = res[0].height+10;
      that.setData({
        // clientHeights: res[0].height+10,
        allHeight: that.data.allHeight,
      })
    })
    //设置swiper最低高度，只做一次初始化
    if(!that.data.minHeightIs) {
      wx.createSelectorQuery().select("#page__bar").boundingClientRect().exec(function(res) {
        // console.log(res)
        // console.log('--屏幕高度568--'+app.globalData.screenHeight)
        // console.log('--窗口高度520--'+app.globalData.windowHeight)
        var height1 = app.globalData.windowHeight;
        that.setData({
          swiperMinHeight_: height1 - res[0].height-10,
          minHeightIs: true,
        })
      })
    }
    
  },
  /**
   * 监听页面下拉动作
   */
  onPullDownRefresh: function() {

    var str = utils.trim(this.data.inputVal);//去空
    var fData = {};//请求参数
    if(str==='') {
      fData = {pageIndex: 1,pageSize: this.data.pageSize,type:this.data.selectType}
    }else{
      fData = {pageIndex: 1,pageSize: this.data.pageSize,s: str,type:this.data.selectType}
    }
    this.data.allImgList[this.data.currentTab].leftList=[];
    this.data.allImgList[this.data.currentTab].rightList=[];
    this.data.allImgList[this.data.currentTab].pageNo=1;
    this.data.allImgList[this.data.currentTab].itemCount=0;
    this.setData({
      allImgList: this.data.allImgList,
    })

    this.queryData(fData,this.data.currentTab);//查询数据
  },
  /**
   * 监听页面上拉触底事件
   */
  onReachBottom:function() {
    var that = this;
    var allImgList = that.data.allImgList;

    var pageNum = allImgList[that.data.currentTab].pageNo;
    var itemCountPage = allImgList[that.data.currentTab].itemCount;
    //设置下一页
    allImgList[that.data.currentTab].pageNo = pageNum + 1;
    if(pageNum < itemCountPage) {
      that.setData({
        allImgList: allImgList
      })

      var str = utils.trim(that.data.inputVal);//去空
      var fData = {};//请求参数
      if(str==='') {
        fData = {pageIndex: allImgList[that.data.currentTab].pageNo,pageSize: that.data.pageSize,type:that.data.selectType}
      }else{
        fData = {pageIndex: allImgList[that.data.currentTab].pageNo,pageSize: that.data.pageSize,s: str,type:that.data.selectType}
      }

      that.queryData(fData,that.data.currentTab);//查询数据
    } else {
      wx.showToast({
        title: '没有更多数据了',
        icon: 'none',
        duration: 1000,
        mask: true
      })
    }
  },
  // 重新加载数据
  refreshData: function (){

    this.queryData({pageIndex: 1,pageSize: 10,type:this.data.selectType},this.data.currentTab,'refresh');
  },
  showInput: function () {//搜索框
    this.setData({
        inputShowed: true
    });
  },
  hideInput: function () {//取消搜索
      this.setData({
          inputVal: "",
          inputShowed: false
      });
      this.refreshData();
  },
  clearInput: function () {//清空搜索
      this.setData({
          inputVal: ""
      });
  },
  inputTyping: function (e) {
      this.setData({
          inputVal: e.detail.value
      });
  },
  //搜索请求
  goSearch: function (e) {
    var str = e.detail.value.replace(/\s+/g, '');//去空
    var fData = {};//请求参数
    if(str==='') {
      fData = {pageIndex: 1,pageSize: 10,type:this.data.selectType}
    }else{
      fData = {pageIndex: 1,pageSize: 10,s: e.detail.value,type:this.data.selectType}
    }
    this.data.allImgList[this.data.currentTab].leftList=[];
    this.data.allImgList[this.data.currentTab].rightList=[];
    this.data.allImgList[this.data.currentTab].pageNo=1;
    this.data.allImgList[this.data.currentTab].itemCount=0;
    this.setData({
      allImgList: this.data.allImgList,
    })
    
    this.queryData(fData,this.data.currentTab);
  },
  //点击图片，跳转详情页
  golist: function(e) {
    wx.getSetting({
      success(res) {
        //用户是否授权
        if (!res.authSetting['scope.userInfo'] ||!app.globalData.session_key || app.globalData.session_key=="") {
          wx.navigateTo({
            url: '../authorize/login/index',
          })
        }else{
          //已经授权
          var pid = e.currentTarget.dataset.pid;//图片id
          var desc = e.currentTarget.dataset.desc;//图片描述
          var auth = e.currentTarget.dataset.auth;//是否为权限图片 1是 0否（权限图片需要权限等级足够才能无水印预览）
          wx.navigateTo({
            url: 'list/index?pid='+pid+'&desc='+desc+'&auth='+auth,
          })
        }
      }
    })

  },
  //返回顶部
  goTop: function() {
    this.setData({
      showTop: true,
    })
    wx.pageScrollTo({
      duration: (300),
      scrollTop: 0,
    })
  },
  //页面滚动
  onPageScroll: function(e) {
    this.showgoTop(e)
  },
  //回到顶部按钮显示
  showgoTop(obj) {
    clearTimeout(this.scrollTimer);
    this.scollArr = Array.isArray(this.scollArr) ? this.scollArr : []; //记录滚动坐标
    this.scollArr.push(obj.scrollTop);
    if (this.scollArr.length > 2) {
       //1向下滚动  -1向上滚动
      this.direction = (this.scollArr[this.scollArr.length - 1] - this.scollArr[0]) > 0 ? 1 : -1;
      //向下滚动且离顶部2000px
      if(this.direction==1 && obj.scrollTop > 500) {
        this.setData({
          showTop: false,
        })
      }else{
        this.setData({
          showTop: true,
        })
      }
    }
    this.scrollTimer = setTimeout(() => {//延迟清除记录
      this.scollArr = [];
    }, 100)
  },
    //显示密码框
    toCheck: function(e) {
      let that = this;
      /**
       * 这里做私密图片口令校验
       */
      var isprivate = e.currentTarget.dataset.isprivate;
      if(isprivate && isprivate==1) {
        that.setData({
          showModal: true,
          clickObj: e
        });
      }
    },
  //绑定密码输入值
  inputChange: function(e) {
    this.setData({
      inputValue: e.detail.value
    })
  },
  showDialogBtn: function() {
    this.setData({
      showModal: true
    })
  },
  preventTouchMove: function () {
  },
  hideModal: function () {
    this.setData({
      showModal: false
    });
  },
  //取消
  onCancel: function () {
    this.hideModal();
  },
  //确定
  onConfirm: function () {
    var val = this.data.inputValue;
    var pwd = this.data.clickObj.currentTarget.dataset.passwd;
    if(pwd===val || val===app.globalData.managerPwd ) {
      this.hideModal();
      this.golist(this.data.clickObj);
    }else{
      wx.showToast({
        title: "口令错误",
        duration: 1000,
        icon: 'none',
        mask: true
      })
    }
  },
    /**
   * 用户点击右上角分享
   */
  onShareAppMessage: function (option) {
    var that = this;
    return {
      title: app.globalData.miniName,
      path: '/pages/index/index?flag=share',
      imageUrl: '../../images/mini.jpg'

    }
  },
  /**
   * 用户点击右上角分享朋友圈
   */
  onShareTimeline: function(res) {
    var that = this;
    return {
      title: app.globalData.miniName,
      query: 'index=share',
      // imageUrl: '../../../images/mini.jpg'
    }
  },
})

