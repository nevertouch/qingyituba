// pages/index/list/list.js
const app = getApp();
const util = require("../../index/common.js");
Page({

  /**
   * 页面的初始数据
   */
  data: {

    //父级主键
    parentid:'',

    //最新一页评论
    commentData:[],

    //总评论数
    commentSum:0,

    //评论总页码
    commentPages:0,

    //顶部navbar
    nvabarData: {
      showCapsule: 1, //是否显示左上角图标   1表示显示    0表示不显示
      title: '图片详情', //导航栏 中间的标题
      bgColors: '-webkit-linear-gradient(top,rgba(0,0,0,.5),rgba(0,0,0,0))'
    },
    // 此页面 页面内容距最顶部的距离
    height: app.globalData.height * 2 + 20 ,

    //进入页面是否第一次打开评论面板
    firstOpenComment: true,

    //是否收藏
    isFavor: false,


    //图片列表
    piclist: [
      {"path": "../img/4.jpg","coverHeight": 408,"coverWidth": 612},
      {"path": "../img/5.jpg","coverHeight": 405,"coverWidth": 612},
      {"path": "../img/1.jpg","coverHeight": 408,"coverWidth": 612},
      {"path": "../img/2.jpg","coverHeight": 145,"coverWidth": 300},
      {"path": "../img/8.jpg","coverHeight": 333,"coverWidth": 500}
    ],
    imgUrls: []

  },

  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options) {
    var authLevel = app.globalData.authorization.authLevel;
    var that = this;

    var pid = options.pid;//父级主键（页面跳转获取）
    var desc = options.desc;
    var auth = options.auth;

    that.setData({
      desc: desc,
      parentid: pid,
      //登录态
      loginStatus: app.globalData.session_key,
      previewImgIs: !(auth==1) || (authLevel==1 || authLevel==2 || authLevel==3 || authLevel==6),
      waterMarkIs: !(auth==1) ||  (authLevel==1 || authLevel==2 || authLevel==3 || authLevel==6),
      authLevel: authLevel,
    })

    util.post(app.globalData.URL+'/mini/imglist/'+pid+'/'+desc,
    {
      skey:app.globalData.session_key
    }).then(function(res) {
      if(res.data) {
        var obj = res.data;
        if(obj.status && obj.status=="noLogin") {
          wx.navigateTo({
            url: '../../authorize/login/index',
          })
          return;
        }

        that.setData({
          imgUrls: obj.urls,
          imgCount: obj.imgCount,
          // desc: obj.description,
          commentSum: obj.commentSum,
        })

      }else{
        wx.showToast({
          title: "没有更多数据了",
          duration: 1000,
          icon: 'none',
          mask: true
        })
      }
    },function(err) {
      wx.showToast({
        title: "请求失败！",
        duration: 1000,
        icon: 'none',
        mask: true
      })
    })

  },
  onReady: function(){
  },
  //图片预览
  imgPreview: function (e) {
    
    //是否有预览权限
    if(!this.data.previewImgIs) {
      this.selectComponent("#top-showinfo").showAnima();
      return;
    }

    var src = e.currentTarget.dataset.imgpath;
    var imgList = this.data.imgUrls;//获取data-list
    //图片预览
    wx.previewImage({
      current: src, // 当前显示图片的http链接
      urls: imgList // 需要预览的图片http链接列表
    })
  },

  //打开评论面板
  showComment: function() {
    var that = this;
    that.selectComponent("#commentlist").showTalks();
    //获取第一页评论
    if(that.data.firstOpenComment) {
      //如果是初次打开评论，则初始化第一页数据
      that.getPageComment();
      that.setData({
        firstOpenComment: false,//是否第一次打开评论
      })
    }
  },

  //获取下一页评论，从子组件调用
  getPageComment: function(e) {
    var pageNo = 1;
    if(e) {
      pageNo = e.detail.pageNo;
    }
    var that = this;
    // 评论所属主题id
    var themeid = that.data.parentid;
    var pageSize = 10;//每页大小
    util.post(app.globalData.URL+'/mini/listComments/'+themeid+'/'+pageSize+'/'+pageNo,{}).then(
      function(res) {
        if(res.data.list.length > 0) {
          that.setData({
            commentData: res.data.list,
            commentSum: res.data.count,
            commentPages: res.data.pageTotal,
          })
        }else{
          wx.showToast({
            title: "没有更多评论了",
            duration: 1000,
            icon: 'none',
            mask: true
          })
        }
        
      },
      function(error) {
      }
    )

  },
  //发送评论，从子组件调用
  sendComments: function(e) {
    var that = this;
    //用户是否授权
    wx.getSetting({
      success(res) {
        if (!res.authSetting['scope.userInfo']) {
          wx.navigateTo({
            url: '../../authorize/login/index',
          })
        }else{
          //已经授权，继续提交评论
          var comments = e.detail.commentStr;
          if(util.trim(comments)==="") {
            wx.showToast({
              title: '请输入内容哦~',
            })
            return;
          }
          var skey = app.globalData.session_key;
          var params = app.globalData.URL+'/mini/saveComment?'+"themeid="+that.data.parentid+"&themename=图片评论"+"&comment="+comments+"&skey="+skey;
          //提交到服务器
          util.post(encodeURI(params),{}).then(
            function(res) {
              var resObj = res.data;
              //校验不通过
              if(resObj.res==='risky') {
                wx.showToast({
                  title: "校验不通过，内容可能包含敏感信息！",
                  duration: 1500,
                  icon: 'none',
                  mask: true
                })
                return;
              }
              //校验通过
              var addTalks={
                avatarUrl: resObj.avatarurl,
                nickName: resObj.nickname,
                content: resObj.content,
                talkTime: resObj.createtime,
                likeCount: resObj.likeCount,
                id: resObj.id,
                loginStatus: resObj.loginStatus,
              }
              //将评论添加到最前方
              that.selectComponent("#commentlist").newOneComment(addTalks);
              //评论成功，更新总评论数
              that.setData({
                commentSum: that.data.commentSum+1,
                loginStatus: app.globalData.session_key,
              })
            },
            function(error) {
            }
          )

        }
      }
    })

   

  },
  //删除评论
  deleteComment: function(e) {
    var that = this;
    var commentid = e.detail.commentid;
    var commentIndex = e.detail.commentIndex;
    var authLevel = e.detail.authLevel;

    //提交到服务器
    util.post(app.globalData.URL+'/mini/deleteComment/'+commentid+'/'+app.globalData.session_key+'/'+authLevel,{}).then(
      function(res) {
        var resObj = res.data;
        if(resObj.res === 'ok') {
          //调用组件方法移除该数据
          that.selectComponent("#commentlist").delOneComment(commentIndex);
          wx.showToast({
            title: "删除成功",
            duration: 1000,
            icon: 'none',
            mask: true
          })
          //删除成功，更新总评论数
          that.setData({
            commentSum: that.data.commentSum-1
          })
        }else{
          wx.showToast({
            title: "删除失败",
            duration: 1000,
            icon: 'none',
            mask: true
          })
        }
      },
      function(error) {
        wx.showToast({
          title: "网络异常，请稍后再试！",
          duration: 1000,
          icon: 'none',
          mask: true
        })
      }
    )
  },
  reportPl: function(e) {
    var that = this;
    var commentid = e.detail.commentid;

    //提交到服务器
    util.post(app.globalData.URL+'/mini/reportComment/'+commentid,{}).then(
      function(res) {
        var resObj = res.data;
        if(resObj.res === 'ok') {
          wx.showToast({
            title: "已提交等待审核！",
            duration: 1000,
            icon: 'none',
            mask: true
          })
        }else{
          wx.showToast({
            title: "提交出错，请稍后再试！",
            duration: 1000,
            icon: 'none',
            mask: true
          })
        }
      },
      function(error) {
        wx.showToast({
          title: "网络异常，请稍后再试！",
          duration: 1000,
          icon: 'none',
          mask: true
        })
      }
    )
  },
  //跳到首页
  goIndex: function() {
    wx.navigateBack()
  },
  //收藏
  setFavor: function() {
    this.setData({
      isFavor : !this.data.isFavor
    })
    wx.showToast({
      title: '该功能尚未开发',
    })
  },
  /**
   * 用户点击右上角分享
   */
  onShareAppMessage: function (option) {
    var that = this;
    return {
      title: that.data.desc,
      path: '/pages/index/index',
      imageUrl: that.data.imgUrls[0]

    }
  },
  
})