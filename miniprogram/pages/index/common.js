
//数组中是否存在某元素
function defineIndexOf(str, val) {
  return str.indexOf(val);
 }

 //删除数组元素
 function remove(arr,val) {
  var array = Array.from(new Set(arr));
  for (var index = 0; index < array.length; index++) {
    if (array[index] == val) {
      array.splice(index,1)
    }
  }
  return array;
 }
 

//正则判断
function Regular(str, reg) {
 if (reg.test(str))
  return true;
 return false;
}

//是否为中文
function IsChinese(str) {
 var reg = /^[\u0391-\uFFE5]+$/;
 return Regular(str, reg);
}
//去左右空格;
function trim(s){
  return s.replace(/(^\s*)|(\s*$)/g, "");
}
//字符串转json对象
function str2json(str) {
  return JSON.parse(str);
}
//json对象转字符串
function json2str(obj) {
  return JSON.stringify(obj);
}


var get = function (url) {
  var promise = new Promise((resolve, reject) => {
      var that = this;
      wx.request({
       url: url,
       header: { 'content-type': 'application/json' },
       success: function (res) {
          resolve(res); 
       },
       error: function (e) {
          reject('网络出错');
       }
      })
  });
return promise;
}

var post=function(url, data,_header) {
  var promise = new Promise((resolve, reject) => {
    // var that = this;
    var headerStr = { 'content-type': 'application/x-www-form-urlencoded' };
    if(!_header || _header==undefined || _header==='') {
      _header=headerStr;
    }
    var postData = data;
      wx.request({
        url: url,
        data: postData,
        header: _header,
        method: 'POST',
        success: function (res) {
          resolve(res);
        },
        error: function (e) {
          reject('网络出错');
        }
      })
    });
  return promise;
}



//最下面一定要加上你自定义的方法（作用：将模块接口暴露出来），否则会报错：util.trim is not a function;
module.exports = {
  IsChinese: IsChinese,
  trim: trim,
  indexOf:defineIndexOf,
  remove:remove,
  post:post,
  get:get,
  str2json: str2json,
  json2str: json2str
}