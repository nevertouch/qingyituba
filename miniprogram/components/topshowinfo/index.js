
Component({

  behaviors: [],

  properties: {
    msg:{
      type:String,
      value:'您没有权限查看哦！'
    }
  },
  
  data: {

  }, // 私有数据，可用于模板渲染

  //组件方法
  methods:{

    //进行提示
    showAnima: function (e) {
      if(!this.data.show){
        let that = this;
        this.setData({
          show: 1
        })
        setTimeout(function () {
          that.setData({
            show: 0
          })
        }, 2000)
      }
    },

  }


})
