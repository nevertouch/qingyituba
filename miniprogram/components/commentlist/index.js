
Component({

  behaviors: [],

  options: {
    addGlobalClass: true,
    // pureDataPattern: /^pure_/, //以pure_ 开头的数据字段为纯数据字段，仅仅记录在this.data 不参与任何界面渲染过程  
  },

  properties: {
    //最新一页评论
    _commentData: {
      type: Object,
      value: [],
      //组件数据监听器
      observer: function(newValue,oldValue) {

        if(newValue && newValue.length>0) {
          this.loadTalks(newValue);
        }
      }
    },
    //总评论数量
    _commentSum: Number,
    //总页数
    _commentPages: Number,
    //登录态sessionid
    _loginStatus: String,
    //权限等级
    _authLevel: Number,
  },
  
   // 私有数据，可用于模板渲染
  data: {

    talks: [],

    pageNo:1,//页数
    pageSize:10,//条数

    commentInput:'', //输入框的值

    showIOSDialog: false,//评论操作框显示-IOS
    showAndroidDialog: false,//评论操作框显示-Android
    canDelete: false,

    showArea: true,//是否切换显示textarea
    showInput: false,

  },

  lifetimes: {
    //组件生命周期函数-在组件实例进入页面节点树时执行
    attached: function() {
    },
    //组件生命周期函数-在组件布局完成后执行
    ready: function() {
      // 评论弹出层动画创建
      this.animation = wx.createAnimation({
        duration: 400, // 整个动画过程花费的时间，单位为毫秒
        timingFunction: "ease", // 动画的类型
        delay: 0 // 动画延迟参数
      })
    },
  },

  


  // 组件所在页面的生命周期函数
  pageLifetimes: {
    show: function () { },// 页面被展示
    hide: function () { },// 页面被隐藏
    resize: function () { },// 页面尺寸变化
  },

  methods: {

    //打开评论
    showTalks: function () {

      // 设置动画内容为：使用绝对定位显示区域，高度变为100%
      this.animation.bottom("0rpx").height("100%").step()
      
      this.setData({
        talksAnimationData: this.animation.export()
      })
    },
  
    //收起评论面板
    hideTalks: function () {
      // 设置动画内容为：使用绝对定位隐藏整个区域，高度变为0
      this.animation.bottom("-100%").height("0rpx").step()
      this.setData({
        // talks: [],
        talksAnimationData: this.animation.export()
      })
    },
  
    // 加载数据
    loadTalks: function (data) {
      wx.showNavigationBarLoading();
      var that = this;
      var talks = [];
      
      talks = this.data.talks.concat(data);

      this.setData({
        talks: talks,
        talksAnimationData: that.animation.export(),
        pageNo: this.data.pageNo+1
      })
      wx.hideNavigationBarLoading();
    },

    //不用
    _areaBlur: function(e) {
      // let that = this;
      // that.setData({
      //   inputHeight: 0,
      //   showInput: false,
      //   showArea: true
      // })
    },

    //输入框聚焦时（不用）
    _inputFocus: function (e){
      // let that = this;
      // // console.log(e,'键盘弹起')
      // var inputHeight = 0
      // if (e.detail.height) {
      //   inputHeight = e.detail.height
      // }
      
      // wx.showToast({
      //   title: '键盘高度：'+inputHeight,
      // })
      // that.setData({
      //   inputHeight: inputHeight,
      //   showInput: true,
      //   showArea: false
      // })
  
    },
    //绑定输入框输入事件
    inputedit: function(e) {
      let _this = this;
      _this.setData({
        commentInput: e.detail.value
      })

    },
  
    //评论上拉触底
    _onScrollLoad: function (e) {
      //绑定触发事件getComment,从父组件获取最新一页评论，通过组件属性传递到组件内部
      var params = {'pageNo': this.data.pageNo}

      if(this.data.pageNo > this.properties._commentPages) {
        wx.showToast({
          title: "没有更多评论了",
          duration: 1000,
          icon: 'none',
          mask: true
        })
        return;
      }

      var myEventOption = {} //触发事件的选项
      this.triggerEvent('getComment',params,myEventOption)

    },
    moveStop: function() {
      //这里的滚动方法只是用来阻止评论面板事件冒泡
    },

    //发送评论
    sendComments: function() {
      var commentStr = this.data.commentInput;
      //绑定触发事件sendComment,调用父组件方法发送评论
      var params = {'commentStr': commentStr}
      var myEventOption = {} //触发事件的选项
      this.triggerEvent('sendComment',params,myEventOption)
      //清空评论框
      this.setData({
        commentInput: ''
      })
    },
    //将评论添加到面板第一条
    newOneComment: function(newOne) {
      var newComment = this.data.talks;
      newComment.unshift(newOne);
      this.setData({
        talks: newComment
      })
    },
    // 移除数据中要删除的评论，并进行setData
    delOneComment: function(selectIndex) {
      wx.showToast({
        title: '删除成功',
      })
      var newComment = this.data.talks;
      newComment.splice(selectIndex,1);//根据数组下标移除
      this.setData({
        talks: newComment
      })
    },
    //长按评论触发
    commentAction: function(e) {
      var id = e.currentTarget.dataset.id;//ID
      var text = e.currentTarget.dataset.text;//内容
      var index = e.currentTarget.dataset.index;//下标
      var skey = e.currentTarget.dataset.skey;//登录态

      //数据中登录态跟storage一样则是本人评论，或者权限等级为1、2，显示删除选项
      var _canDelete = false;
      var _authLevel = this.properties._authLevel;
      if(this.properties._loginStatus === skey || (_authLevel==1 || _authLevel==2)) {_canDelete=true }
      this.setData({
        showAndroidDialog: true,
        selectText: text,
        selectId: id,
        selectIndex: index,
        canDelete: _canDelete,
      });

    },
    actionClose: function() {//关闭
      this.setData({
          showIOSDialog: false,
          showAndroidDialog: false
      });
    },
    copyComment: function() {//复制
      var that = this;
      wx.setClipboardData({
        data: that.data.selectText,
        success (res) {
          // wx.getClipboardData({
          //   success (res) {
          //     console.log(res.data) // data
          //   }
          // })
        }
      })
    },
    //评论举报
    reportComment: function() {
      var that = this;
      //绑定触发事件reportPl,调用父组件方法删除评论
      var params = {'commentid': that.data.selectId}
      var myEventOption = {} //触发事件的选项
      that.triggerEvent('reportPl',params,myEventOption)
    },
    delComment: function() {//删除
      var that = this;
      //绑定触发事件delComment,调用父组件方法删除评论
      var params = {'commentid': that.data.selectId,'commentIndex': that.data.selectIndex,'authLevel':this.properties._authLevel}
      var myEventOption = {} //触发事件的选项
      that.triggerEvent('delComment',params,myEventOption)
    }
  }

})